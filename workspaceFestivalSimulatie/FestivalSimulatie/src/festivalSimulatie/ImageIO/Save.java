package festivalSimulatie.ImageIO;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import festivalSimulatie.Panels.Item;


public class Save 
{
	public void saveFile(ArrayList<Item> itemsForView, ArrayList<Item> items) 
	{
		JFileChooser saver = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Festival Simulator Data", "fsd");
		saver.setFileFilter(filter);

		int returnVal = saver.showSaveDialog(SwingUtilities
				.getWindowAncestor(saver));
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			SaveStream savestream = new SaveStream();

			File file = new File(saver.getSelectedFile()+".fsd");
			//savestream.saveToFile(file,itemsForView,items);
			savestream.saveToFile(file, itemsForView, items);
		}
	}

}
