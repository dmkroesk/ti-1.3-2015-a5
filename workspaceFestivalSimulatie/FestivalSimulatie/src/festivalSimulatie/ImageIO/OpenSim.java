package festivalSimulatie.ImageIO;

import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import festivalSimulatie.Panels.Item;

public class OpenSim  {

	public ArrayList<Item> itemsForView;
	public ArrayList<Item> items;

	public void openFile() throws ClassNotFoundException {
		JFileChooser opener = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Festival Simulator Data", "fsd");
		opener.setFileFilter(filter);

		int returnVal = opener.showOpenDialog(SwingUtilities.getWindowAncestor(opener));
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			if(opener.getSelectedFile() != null){
				LoadStreamSim loadstream = new LoadStreamSim();
				loadstream.readFile(opener.getSelectedFile());
				this.itemsForView = loadstream.getItemsForView();
				this.items = loadstream.getItems();
			}
			else{
				items = null;
			}
			if(returnVal == JFileChooser.CANCEL_OPTION){items = null;}
		}
	}

	public ArrayList<Item> getItemsForViewObject(){
		return itemsForView;
	}
	public ArrayList<Item> getItemsObject(){
		return items;
	}


}
