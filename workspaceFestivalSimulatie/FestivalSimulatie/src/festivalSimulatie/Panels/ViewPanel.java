package festivalSimulatie.Panels;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import festivalSimulatie.ImageIO.ImageLoader;
import festivalSimulatie.Object.DigitalClock;

@SuppressWarnings("serial")
public class ViewPanel extends JPanel implements ActionListener {
	
	float cameraScale = 1;
	BufferedImage background;
	BufferedImage background2;
	BufferedImage Image;
	ArrayList<Item> objects = BuildingPanel.getViewableItems();
	Item dragObject = null;
	Image Image2;
	Point2D lastClickPosition;
	Point2D cameraPoint = new Point2D.Double(0, 0);
	Point lastMousePosition;

	
	private double left = -200;
	private double top = -200;


	Timer timer;
	DigitalClock clock;
	

	public ViewPanel(BuildingPanel buildingpanel) {
		super();
		setBounds(10, 10, 751, 500);
		setLayout(new BorderLayout());
		add(new ViewPanel());
		timer = new Timer(1000/60,this);
		clock = new DigitalClock(timer);		
	}

	ViewPanel() {
		try {
			background = ImageIO.read(new File("res/grass.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			background2 = ImageIO.read(new File("res/asphalt.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Point2D clickPoint = getClickPoint(e.getPoint());
				lastClickPosition = clickPoint;
				lastMousePosition = e.getPoint();
				objects = BuildingPanel.getViewableItems();
				for (Item o : objects)
					if (o.contains(clickPoint)) {
						dragObject = o;
						OptionsPanel.setPanel(o.getBuilding());
					}
			}

			public void mouseReleased(MouseEvent e) {
				dragObject = null;
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {


			public void mouseDragged(MouseEvent e) {
				Point2D clickPoint = getClickPoint(e.getPoint());
				if (dragObject != null) {
						
					if (SwingUtilities.isLeftMouseButton(e)) {
						dragObject.oldPosition = dragObject.position;
						dragObject.position = new Point2D.Double(
								dragObject.position.getX()
										- (lastClickPosition.getX() - clickPoint
												.getX()),
								dragObject.position.getY()
										- (lastClickPosition.getY() - clickPoint
												.getY()));
						if (dragObject.position.getX() > getWidth()+300 )
						{
							dragObject.setOldPosition();
						}
						if (dragObject.position.getX() < -getWidth()/2 )
						{
							dragObject.setOldPosition();
						}
						if (dragObject.position.getY() > getHeight()+600 )
						{
							dragObject.setOldPosition();
						}
						if (dragObject.position.getY() < -getHeight()/2 )
						{
							dragObject.setOldPosition();
						}
						for (Item i : objects) {
							if (i != dragObject) {
							
							if (dragObject.contains(i.position)
										|| dragObject
												.contains(new Point2D.Double(
														i.position.getX()
																+ Image2.getWidth(null)*i.scale,
														i.position.getY()
																+ Image2.getHeight(null)*i.scale))
										|| dragObject
											.contains(new Point2D.Double(
														i.position.getX(),
														i.position.getY()
																+ Image2.getHeight(null)*i.scale))
										|| dragObject
												.contains(new Point2D.Double(
														i.position.getX()
																+ Image2.getWidth(null)*i.scale,
														i.position.getY()))
										|| dragObject
												.contains(new Point2D.Double(
														i.position.getX()
																+ (Image2.getWidth(null))*i.scale,
														i.position.getY()
																+ (Image2.getHeight(null)/2)*i.scale))	
										|| dragObject
											.contains(new Point2D.Double(
														i.position.getX(),
														i.position.getY()
																+ (Image2.getHeight(null)/2)*i.scale))
										|| dragObject
												.contains(new Point2D.Double(
														i.position.getX()
																+ (Image2.getWidth(null)/2)*i.scale,
														i.position.getY()))		
														|| dragObject
														.contains(new Point2D.Double(
																i.position.getX()
																		+ (Image2.getWidth(null)/2)*i.scale,
																i.position.getY()
																		+ (Image2.getHeight(null))*i.scale))	
									
										)
									
										 {
									dragObject.setOldPosition();
								}

							}

						}

					} else
						dragObject.rotation += (lastClickPosition.getX() - clickPoint
								.getX());
					repaint();

				} else {
					cameraPoint = new Point2D.Double(

					cameraPoint.getX() + (lastMousePosition.getX() - e.getX()),
							cameraPoint.getY()
									+ (lastMousePosition.getY() - e.getY()));
					if (cameraPoint.getX() < left) {
						cameraPoint = new Point2D.Double(left, cameraPoint.getY());
					}
					if (cameraPoint.getY() < top) { 
						cameraPoint = new Point2D.Double(cameraPoint.getX(), top);
					}
					if (cameraPoint.getY() > (((getHeight()*1.25+300) * cameraScale))) {
						cameraPoint = new Point2D.Double(cameraPoint.getX(),
								(((getHeight()*1.25+300) * cameraScale)));
					}
					if (cameraPoint.getX() > (((getWidth()*1.25+300) * cameraScale))) {
						cameraPoint = new Point2D.Double((((getWidth()*1.25+300) * cameraScale)),
								cameraPoint.getY());
					} 
					else {
						repaint();
					}
				}
				lastMousePosition = e.getPoint();
				lastClickPosition = clickPoint;
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				Point2D clickPoint = getClickPoint(e.getPoint());
				for (Item o : objects) {
					if (o.contains(clickPoint)) {
						o.scale *= 1 + (e.getPreciseWheelRotation() / 10.0);
						
						repaint();
						return;
					}
				}
				e.getPoint();
//				System.out.println(e.getPreciseWheelRotation());
				
				cameraScale *= 1 - (e.getPreciseWheelRotation() / 10.0);
//				System.out.println(cameraScale);
				if (cameraScale > 0.7 && cameraScale < 1.5944049)
				{
					top -= -29 * e.getPreciseWheelRotation();
					left -= -43 * e.getPreciseWheelRotation();
				}
				if(cameraScale == 1  )
				{
					top = -200;
					left = -200;
				}
				if (cameraScale < 0.7) {
					cameraScale = (float) 0.7;
				}
				else{
				if (cameraScale > 1.5944049)
				{
					cameraScale = (float) 1.5944049;
				}
				}
				repaint();
			}
		});

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		AffineTransform oldTransform = g2.getTransform();
		g2.setTransform(getCamera());
		TexturePaint p2 = new TexturePaint(background2, new Rectangle2D.Double(0,
				0, 100, 100));
		g2.setPaint(p2);
		g2.fill(new Rectangle2D.Double(-getWidth(), -getHeight(), 3000,
				3000));
		TexturePaint p = new TexturePaint(background, new Rectangle2D.Double(0,
				0, 100, 100));
		g2.setPaint(p);
		g2.fill(new Rectangle2D.Double(-getWidth() / 2, -getHeight() / 2, 1700,
				1700));

		for (Item o : objects) {
			AffineTransform tx1 = new AffineTransform();
			Image2 = ImageLoader.loadImage(o.getPath());
			tx1.translate(o.position.getX(), o.position.getY());
			tx1.scale(o.scale, o.scale);
			tx1.rotate(Math.toRadians(o.rotation), Image2.getWidth(null) / 2, Image2.getHeight(null) / 2);
			g2.drawImage(Image2, tx1, null);
		}

		g2.setTransform(oldTransform);
	}

	private AffineTransform getCamera() {
		AffineTransform tx = new AffineTransform();
		tx.translate(-cameraPoint.getX() + getWidth() / 2, -cameraPoint.getY()
				+ getHeight() / 2);
		tx.scale(cameraScale, cameraScale);

		return tx;
	}

	private Point2D getClickPoint(Point point) {
		try {
			return getCamera().inverseTransform(point, null);
		} catch (NoninvertibleTransformException e1) {
			e1.printStackTrace();
		}
		return null;
	}

	public void reInit(ArrayList<Item> items) {
		objects = items;
	}

	@Override
	public void actionPerformed(ActionEvent e) {		
		repaint();
		clock.tick();
		System.out.println(clock.getMaxFrames()+" "+clock.getFrames()+" "+clock.getTimer().getDelay());		
	}
	
	public DigitalClock getClock(){
		return clock;
	}
}
