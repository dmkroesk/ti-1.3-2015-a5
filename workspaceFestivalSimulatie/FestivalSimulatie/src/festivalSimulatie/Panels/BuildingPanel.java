package festivalSimulatie.Panels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

import Objecten.Festival;
import festivalSimulatie.ImageIO.ImageLoader;
import festivalSimulatie.Object.Gate;
import festivalSimulatie.Object.StageS;

public class BuildingPanel extends JPanel  {
	private static final long serialVersionUID = 1L;
	private Point point = new Point();
	private ArrayList<Item> items  = new ArrayList<>();
	private static ArrayList<Item> itemsForView;
	private OptionsPanel optionsPanel;
	private int columns,row;
	private Festival festival;

	public BuildingPanel(OptionsPanel optionsPanel, Festival festival)
	{
		setBounds(771, 10, 240, 500);
		this.festival=festival;
		this.optionsPanel=optionsPanel;
		this.itemsForView= new ArrayList<>();
		this.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e){
				point = e.getPoint();
				tick();
			}
		});

		init();
		
	}

	public void tick(){
		for(int i = 0 ; i < items.size(); i++){
			if(items.get(i).getRekt().contains((Point2D) point)){
				System.out.println("found");
				optionsPanel.setPanel(items.get(i).getBuilding());
				if(!items.get(i).isUsed()){
					itemsForView.add(items.get(i));
					if(!items.get(i).getBuilding().isStackable()){
						items.get(i).setUsed(true);
						repaint();
					}
				}
				System.out.println(items.size());
			}
		}
	}


	public void paintComponent(Graphics g){
		super.paintComponent(g);
		System.out.println("x");
		Graphics2D g2 = (Graphics2D)g;
		Rectangle2D.Float whiteScreen = new Rectangle2D.Float(0,0,this.getWidth(),getHeight());
		g2.setColor(Color.LIGHT_GRAY);
		g2.fill((Shape) whiteScreen);
		Color color = new Color(255,0,0,128);

		int row = 0;
		int columns=0;
		for(int i = 0; i < items.size(); i++){
			BufferedImage image = ImageLoader.loadImage(items.get(i).getPath());
			g2.setColor(Color.LIGHT_GRAY);
			g2.fill((Shape)new Rectangle2D.Float((int)20+106*columns,(int)25 + 106*row,96,96));
			g2.drawImage(image,(int)20+106*columns,(int)25 + 106*row,96,96,null);
			if(items.get(i).isUsed()==true){
				System.out.println("xxx");
				g2.setColor(color);
				g2.fill((Shape)new Rectangle2D.Float((int)20+106*columns,(int)25 + 106*row,96,96));
			}
			columns++;
			if(columns==2){
				columns=0;
				row++;
			}
		}
	}
	
	public void clearScreen()
	{
		
	}

	public static ArrayList<Item> getViewableItems(){
		return itemsForView;
	}
	public void reInit(ArrayList<Item> itemsForView, ArrayList<Item> items)
	{
		BuildingPanel.itemsForView=itemsForView;
		this.items=items;
		
		
	
		
		tick();
		
		
		repaint();
		
		System.out.println("asd");
		
	}
	
	
	public void reInit(Festival festival)
	{
		this.festival=festival;
		init();
	}
	
	public void init()
	{
		items=new ArrayList<Item>();
		row = 0;
		columns= 0;
		for(int i = 0 ; i < festival.getStage().size(); i++){
			System.out.println(festival.getStage().get(i).getLineUp().isEmpty());
			if(festival.getStage().get(i).getLineUp().isEmpty()==true)
			{
				StageS stage = new StageS(festival.getStage().get(i).getName(),"/stage31.png", 0, 0,"", "",festival.getStage().get(i),false);
				
				items.add(new Item(new Rectangle2D.Float(20+106*columns,25 + 106*row, 96, 96),"/stage31.png", stage));
				columns++;
				if(columns==2){
					columns=0;
					row++;
				}
			}
			else
			{
				StageS stage = new StageS(festival.getStage().get(i).getName(),"/stage31.png", 0, 0,festival.getStage().get(i).getLineUp().get(0).getTime().getBeginTime(), festival.getStage().get(i).getLineUp().get(0).getTime().getEndTime(),festival.getStage().get(i),false);
				
				items.add(new Item(new Rectangle2D.Float(20+106*columns,25 + 106*row, 96, 96),"/stage31.png", stage));
				columns++;
				if(columns==2){
					columns=0;
					row++;
				}
			}
			//StageS stage = new StageS(festival.getStage().get(i).getName(),"/testPic01-100x100.png", 0, 0,"", "17:00",festival.getStage().get(i),false);
			
			//items.add(new Item(new Rectangle2D.Float(20+106*columns,25 + 106*row, 96, 96),festival.getStage().get(i).getName(),"/testPic01-100x100.png", stage));
			//columns++;
			//if(columns==2){
				//columns=0;
				//row++;
			//}
		}
		items.add(new Item(new Rectangle2D.Float(20+106*columns,25 + 106*row, 96, 96),"/testGate.png",new Gate("GATE", "/testGate.png", 0, 0)));
	}
	public ArrayList<Item>getItems()
	{
		return items;
	}
}
